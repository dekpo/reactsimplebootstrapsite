import React from 'react';
import SwiperCore, { Autoplay, Pagination, Scrollbar, A11y } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';

SwiperCore.use([Autoplay, Pagination, Scrollbar, A11y]);

const Clients = () => (
    <section id="clients" className="clients">
      <div className="container" data-aos="zoom-in">
          <div className="swiper-wrapper align-items-center">
            <Swiper
            spaceBetween={40}
            autoplay
            slidesPerView={3}
            pagination={{ clickable: true }}
            >
            <SwiperSlide>
            <div className="swiper-slide"><img src={require("../assets/img/clients/client-1.png").default} className="img-fluid" alt="" /></div>
            </SwiperSlide>
            <SwiperSlide>
            <div className="swiper-slide"><img src={require("../assets/img/clients/client-2.png").default} className="img-fluid" alt="" /></div>
            </SwiperSlide>
            <SwiperSlide>
            <div className="swiper-slide"><img src={require("../assets/img/clients/client-3.png").default} className="img-fluid" alt="" /></div>
            </SwiperSlide>
            <SwiperSlide>
            <div className="swiper-slide"><img src={require("../assets/img/clients/client-4.png").default} className="img-fluid" alt="" /></div>
            </SwiperSlide>
            <SwiperSlide>
            <div className="swiper-slide"><img src={require("../assets/img/clients/client-5.png").default} className="img-fluid" alt="" /></div>
            </SwiperSlide>
            <SwiperSlide>
            <div className="swiper-slide"><img src={require("../assets/img/clients/client-6.png").default} className="img-fluid" alt="" /></div>
            </SwiperSlide>
            <SwiperSlide>
            <div className="swiper-slide"><img src={require("../assets/img/clients/client-7.png").default} className="img-fluid" alt="" /></div>
            </SwiperSlide>
            <SwiperSlide>
            <div className="swiper-slide"><img src={require("../assets/img/clients/client-8.png").default} className="img-fluid" alt="" /></div>
            </SwiperSlide>
            </Swiper>
          </div>
          <div className="swiper-pagination"></div>
        </div>
    </section>
)

export default Clients