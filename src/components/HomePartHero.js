import React from 'react';

const Hero = () => (
    <section id="hero" className="d-flex align-items-center">

    <div className="container" data-aos="zoom-out" data-aos-delay="100">
      <div className="row">
        <div className="col-xl-6">
          <h1>Better digital experience with React</h1>
          <h2>We are a team of talented developers making websites with React & Bootstrap</h2>
          <a href="#about" className="btn-get-started scrollto">Get Started</a>
        </div>
      </div>
    </div>

  </section>
)

export default Hero